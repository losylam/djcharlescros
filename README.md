Dj Charles Cros
==========

Pitch
----------

Le phonographe à rouleaux est l’un des premiers dispositifs permettant d’enregistrer des sons et de les diffuser.

Inventé simultanément, durant l’année 1877, par le poète et inventeur français Charles Cros (sous le nom de «Paléophone») et par le célèbre ingénieur américain Thomas Edison.
Il aura permis aux linguistes du début du vingtième siècle de réaliser les premières campagnes d’enregistrement de corpus oraux.
Il est également l’ancêtre des gramophones à disque rotatif sur plateau, platines tourne-disques, lecteurs de Compact Disc®, MiniDisc® et autres Blu-ray® et est ainsi à l’origine de l’industrie du disque.

182 ans après l’invention du phonographe à rouleaux, cette installation permet au public d’expérimenter une autre innovation avant gardiste mais méconnue de Charles Cros : le stratching ou turntablism.

Cette pratique musicale aujourd’hui désuète, qui reste pratiquée par quelques nostalgiques, ne se développa en réalité que dans les années 1970, soit près d’un siècle après les premiers essais de Charles Cros, dont les compositions très expérimentales pour l’époque n’auront jamais réussi à séduire le public.


Ressources
----------

http://www.fablabo.net/wiki/Dj_Charles_Cros
http://langues.labomedia.org/
http://wiki.labomedia.org/index.php/Cabinet_de_curiosit%C3%A9s_du_Corpus_de_la_parole

