// =====================================================================
// SuperCollider Workspace
// =====================================================================

s = Server.local.waitForBoot({	
	"booted".postln;
	~serial = SerialPort.new('/dev/serial/by-id/usb-Arduino_LLC_Arduino_Micro-if00', baudrate: 115200);
	~potvel_1 = Bus.control(s,1);
	~potvel_2 = Bus.control(s,1);
	~balance_1 = Bus.control(s,1);
	~balance_2 = Bus.control(s,1);
	~niveau_1 = Bus.control(s,1);
	~niveau_2 = Bus.control(s,1);
	
	~ratio_1 = 37;
	~ratio_2 = 34;

	~n_cum_1 = 16;
	~n_cum_2 = 16;

	~cum_vel_1 = List.new();
	~cum_vel_2 = List.new();


	~n_cum_1.do({ ~cum_vel_1.add(0); ~cum_vel_2.add(0);});
	~n_cum_1_min = 2;
	~n_cum_1_max = 16;

	~n_cum_2_min = 2;
	~n_cum_2_max = 16;

	~buff_1 = Buffer.read(s, "/home/pi/djcharlescros/son/langues_mix.wav");
	~buff_2 = Buffer.read(s, "/home/pi/djcharlescros/son/instru_mix.wav");

	s.sync;

	~serial_routine = Routine({
		var byte, str, res_1, res_2, res_3, res_4, res_5, bal_1, bal_2, dif, lst;
		loop{
			if(~serial.read==10, {
				str = "";
				while({byte = ~serial.read; byte !=13 }, {
					str= str++byte.asAscii;
				});
				lst = Array.new(2);
				lst = str.split($ );
				if (lst.size == 5){
					~cum_vel_1.addFirst(lst[0].asInteger());
					~cum_vel_1.pop();

					res_1 = ~cum_vel_1[0..~n_cum_1].sum()/~n_cum_1;

					if (res_1 < 20, {
						if (~n_cum_1 > ~n_cum_1_min, {
							~n_cum_1 = ~n_cum_1 - 1;
						});
						}, {
							if (~n_cum_1 < ~n_cum_1_max, {
								~n_cum_1 = ~n_cum_1 + 1;
							});
						}
					);

					~cum_vel_2.addFirst(lst[1].asInteger());
					~cum_vel_2.pop();

					res_2 = ~cum_vel_2[0..~n_cum_2].sum()/~n_cum_2;

					if (res_2 < 20, {
						if (~n_cum_2 > ~n_cum_2_min, {
							~n_cum_2 = ~n_cum_2 - 1;
						});
						}, {
							if (~n_cum_2 < ~n_cum_2_max, {
								~n_cum_2 = ~n_cum_2 + 1;
							});
						}
					);
					res_3 = lst[2].asInteger();
					if(res_3 > 50.0){
						bal_1 = 1.0;
						dif = res_3 - 50.0;
						bal_2 = 1- (dif / 50.0);
					};
					if(res_3<=50){
						bal_1 = res_3 / 50.0;
						bal_2 = 1.0;
					};
					res_4 = lst[3].asInteger();
					res_4 = (res_4/100.0);
					res_5 = lst[4].asInteger();
					res_5 = (res_5/100.0);
					~potvel_1.set(res_1.value);
					~potvel_2.set(res_2.value);
					~balance_1.set(bal_1.value);
					~balance_2.set(bal_2.value);
					~niveau_1.set(res_5.value);
					~niveau_2.set(res_4.value);
					//("read value:"+(res_1)+" "+(res_2)+" " + (dif) + " " +(bal_1)+ " " + (bal_2)).postln;
				}
			});
		};
	}).play();

	"serial routine send".postln;
	
	AppClock.sched(10, {
		"synth sched".postln;
		~synth = SynthDef(\scratch, {arg bal_1 = 1.0, bal_2 = 1.0, speed_1 = 0, speed_2 = 0, ratio_1 = 34, ratio_2 = 34, amp_1 = 1, amp_2 = 1, buffer_1, buffer_2, level = 1, pan = 0.5;
			var buf_1, buf_2;
			buf_1 = PlayBuf.ar(1, buffer_1, speed_1 * BufRateScale.kr(buffer_1)/ratio_1, loop: 1);
			buf_2 = PlayBuf.ar(1, buffer_2, speed_2 * BufRateScale.kr(buffer_2)/ratio_2, loop: 1);
			Out.ar(0, buf_1*amp_1*bal_1);
			Out.ar(1, buf_2*amp_2*bal_2);
		}).play();
		//		}).play(Server.local);
	});
	
	AppClock.sched(20, {
		"param sched".postln;
		~synth.set(\buffer_1, ~buff_1);
		~synth.set(\buffer_2, ~buff_2);
		~synth.set(\ratio_1, ~ratio_1);
		~synth.set(\ratio_2, ~ratio_2);
		
		~synth.map(\speed_1, ~potvel_2);
		~synth.map(\speed_2, ~potvel_1);
		
		~synth.map(\amp_1, ~niveau_1);
		~synth.map(\amp_2, ~niveau_2);
		
		~synth.map(\bal_1, ~balance_1);
		~synth.map(\bal_2, ~balance_2);
	});
});

