#define enc11 2
#define enc12 4
#define enc21 3
#define enc22 5

#define pot1 A1
#define pot2 A2
#define pot3 A3

volatile int count_1;
volatile int count_2;
int oldValue = 0;
int oldTime = 0;

int pot1_val;
int pot2_val;
int pot3_val;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  count_1 = 0;
  count_2 = 0;
  pinMode(enc11, INPUT);
  pinMode(enc12, INPUT);
  pinMode(enc21, INPUT);
  pinMode(enc22, INPUT);
  pinMode(pot1, INPUT);
  pinMode(pot2, INPUT);
  pinMode(pot3, INPUT);
  attachInterrupt(digitalPinToInterrupt(enc11), handleEncoder_1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(enc21), handleEncoder_2, CHANGE);
}

void loop() {
  // put your main code here, to run repeatedly:
   noInterrupts();
   /* if ((count_1 >= 30) and (count_1 <= 38)){ */
   /*    Serial.print(34); */
   /* }else{ */
      Serial.print(count_1);
      //}
   Serial.print(' ');
   //   if ((count_2 >= 30) and (count_2 <= 38)){
   //   Serial.print(34);
   //}else{
      Serial.print(count_2);
      //}
   pot1_val = map(analogRead(pot1), 343, 660, 0, 100);
   pot2_val = map(analogRead(pot2), 709, 349, 0, 100);
   pot3_val = map(analogRead(pot3), 297, 694, 0, 100);
   Serial.print(' ');
   Serial.print(pot1_val);
   Serial.print(' ');
   Serial.print(pot2_val);
   Serial.print(' ');
   Serial.println(pot3_val);
    count_1 = 0;
    count_2 = 0;
   interrupts();
//
// int value = count;
// int diff;
// if (value != oldValue){
//   diff = value - oldValue;
//   Serial.print(value);
//   Serial.print('\t');
//   Serial.println(diff);
//   oldValue = value;
////}
//int now = micros();
//int del = now - oldTime;
//oldTime = now;
//if (oldValue != count){

   
//}
// 
 delay(10);
 
}
  
void handleEncoder_1(){
  if (digitalRead(enc11) == digitalRead(enc12)){
  //if (bitRead(PIND, 0)==bitRead(PIND, 2)){
    count_1 ++;
  }else{
    count_1 --; 
  }
}

void handleEncoder_2(){
  if (digitalRead(enc21) == digitalRead(enc22)){
  //if (bitRead(PIND, 1)==bitRead(PIND, 3)){
    count_2 ++;
  }else{
    count_2 --; 
  }
}
